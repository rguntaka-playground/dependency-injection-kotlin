/*
LocalRepository is a Second object that inherits IRepository interface
 */
object LocalRepository: IRepository {
    override fun getData() = "Local Data"
}