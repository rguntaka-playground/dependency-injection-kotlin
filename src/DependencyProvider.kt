class DependencyProvider {
    fun providerRemoteRepo(): IRepository = RemoteRepository
    fun providerLocalRepo(): IRepository = LocalRepository
    fun providerView(): IView = View
}