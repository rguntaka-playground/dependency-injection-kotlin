
/*
Instead of declaring & defining dependencies inside class or constructor,
they are being passed/ injected via constructor arguments
 */

class DecisionMaker(
    val repository: IRepository,
    val view: IView) {
    fun start(){
        view.displayData(repository.getData())
    }
}