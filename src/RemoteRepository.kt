/*
RemoteRepository is a First object that inherits IRepository interface
 */
object RemoteRepository: IRepository {
    override fun getData(): String {
        return "From Remote Repo"
    }
}