
/*
Demonstrating Dependency Injection by passing RemoteRepo or LocalRepo into DecisionMaker constructor
Here, the behavior of DecisionMaker class changes without touching its code
 */

fun main(args: Array<String>){
    val provider = DependencyProvider()

    DecisionMaker(
        provider.providerRemoteRepo(),
        provider.providerView()
    ).start()

    DecisionMaker(
        provider.providerLocalRepo(),
        provider.providerView()
    ).start()
}